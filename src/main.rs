use sexpr::*;
fn main() {
    use SExpr::*;
    println!("{:?}",
      List(vec![
           Identifier("hello".to_string()),
           Symbol("name".to_string()),
           Identifier("world".to_string())]));
}
