use std::ops::*;
use std::convert::From;

#[derive(Debug)]
pub enum SExpr {
  Identifier(String),
  Symbol(String),
  List(Vec<SExpr>)
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Sign { Positive, Negative }
use Sign::*;

#[derive(Debug, Clone, PartialEq)]
pub struct BigInt {
  sign: Sign,
  digits: Vec<u8>
}


impl From<u8> for BigInt {
  fn from(val: u8) -> BigInt {
      BigInt {sign: Positive, digits: vec![val]}
  }
}

impl From<i32> for BigInt {
  fn from(val: i32) -> BigInt {
    let sign = if val < 0 { Negative } else { Positive };
    let mut digits = Vec::new();
    let mut remainder = val;
    while remainder != 0 {
        digits.push((remainder % 256).abs() as u8);
        remainder /= 256;
    }
    BigInt {sign, digits}
  }
}

impl AddAssign<&BigInt> for BigInt {
  fn add_assign(&mut self, other: &BigInt) {
    if self.sign == other.sign {
      let mut carry: u8 = 0;
      for ix in 0 .. usize::max(self.digits.len(), other.digits.len()) {
        let lhs = self.digits.get(ix).cloned().unwrap_or(0) as u16;
        let rhs = other.digits.get(ix).cloned().unwrap_or(0) as u16;
        let add_result = lhs + rhs + (carry as u16);
        carry = (add_result >> 8) as u8;
        let digit = (add_result & 0xff) as u8;
        if ix < self.digits.len() {
          self.digits[ix] = digit;
        } else {
          self.digits.push(digit);
        }
      }
    } else {
      let mut carry: u8 = 0;
      let mut flipped_sign = false;
      for ix in 0 .. usize::max(self.digits.len(), other.digits.len()) {
        let lhs = self.digits.get(ix).cloned().unwrap_or(0) as i16;
        let rhs = other.digits.get(ix).cloned().unwrap_or(0) as i16;
        let add_result = lhs - rhs - (carry as i16);
        if !flipped_sign && ix + 1 >= self.digits.len() && add_result < 0 {
          self.sign = if self.sign == Positive { Negative } else { Positive };
          flipped_sign = true;
        }
        let add_result_abs = add_result.abs();
        carry = (add_result_abs >> 8) as u8;
        let digit = (add_result_abs & 0xff) as u8;
        if ix < self.digits.len() {
          self.digits[ix] = digit;
        } else {
          self.digits.push(digit);
        }
      }
    }
    while self.digits.len() > 0 && self.digits[self.digits.len()-1] == 0 {
      self.digits.pop();
    }
  }
}

#[cfg(test)]
mod tests {
    use crate::BigInt;
    #[test]
    fn ten_plus_ten_is_twenty() {
      let mut lhs = BigInt::from(10);
      let rhs = BigInt::from(10);
      lhs += &rhs;
      assert!(lhs == BigInt::from(20));
    }

    #[test]
    fn ten_plus_zero_is_still_ten() {
      let mut lhs = BigInt::from(10);
      let rhs = BigInt::from(0);
      lhs += &rhs;
      assert!(lhs == BigInt::from(10));
    }

    #[test]
    fn minus_ten_plus_zero_is_still_minus_ten() {
      let mut lhs = BigInt::from(-10);
      let rhs = BigInt::from(0);
      lhs += &rhs;
      assert!(lhs == BigInt::from(-10));
    }

    #[test]
    fn adding_19831_to_minus_21831_yields_minus_2000() {
      let mut lhs = BigInt::from(19831);
      println!("LHS: {:?}", lhs);
      let rhs = BigInt::from(-21831);
      println!("RHS: {:?}", rhs);
      lhs += &rhs;
      println!("LHS+RHS: {:?}", lhs);
      println!("-2000: {:?}", BigInt::from(-2000));
      assert!(lhs == BigInt::from(-2000));
    }

    #[test]
    fn minus_ten_plus_minus_ten_is_minus_twenty() {
      let mut lhs = BigInt::from(-10);
      let rhs = lhs.clone();
      lhs += &rhs;
      assert!(lhs == BigInt::from(-20));
    }

    #[test]
    fn ten_plus_minus_ten_is_zero() {
      let mut lhs = BigInt::from(10);
      let rhs = BigInt::from(-10);
      lhs += &rhs;
      assert!(lhs == BigInt::from(0));
    }

    #[test]
    fn ten_thousand_plus_thirty_two_thousand_is_fourty_two_thousand() {
      let mut lhs = BigInt::from(10000);
      let rhs = BigInt::from(32000);
      lhs += &rhs;
      assert!(lhs == BigInt::from(42000));
    }

    #[test]
    fn ten_is_not_equal_to_ten_thousand() {
      assert!(BigInt::from(10) != BigInt::from(10000));
    }

}


